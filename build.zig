const std = @import("std");

const name = "punycode";

pub fn build(b: *std.Build) void {
    const mode = b.standardOptimizeOption(.{});
    const target = b.standardTargetOptions(.{});
    const exe = b.addExecutable(.{
        .name = "main",
        .root_source_file = .{ .path = "src/main.zig" },
        .optimize = mode,
        .target = target,
    });
    const tests = b.addTest(
        .{
            .root_source_file = .{ .path = "src/punycode.zig" },
        },
    );
    _ = b.addModule(
        "punycode",
        .{
            .root_source_file = .{ .path = "src/punycode.zig" },
        },
    );

    b.default_step.dependOn(&exe.step);
    b.installArtifact(exe);

    const run_step = b.step("run", "Run " ++ name);
    const test_step = b.step("test", "Run all " ++ name ++ " tests");
    const run_cmd = b.addRunArtifact(exe);

    const run_tests = b.addRunArtifact(tests);

    run_step.dependOn(&run_cmd.step);
    test_step.dependOn(&run_tests.step);
}
