const punycode = @import("punycode.zig");
const std = @import("std");

pub fn main() !void {
    var in: [7]u21 = .{ 'h', 'r', 'e', 'i', 'ð', 'u', 'r' };
    var out: [10]u8 = undefined;
    const puny = try punycode.encode(&in, &out);
    try std.io.getStdOut().writer().print("{s}\n", .{puny});
}
