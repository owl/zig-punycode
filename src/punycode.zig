comptime {
    @export(cEncode, .{ .name = "punycode_encode", .linkage = .strong });
    @export(cDecode, .{ .name = "punycode_decode", .linkage = .strong });
}

fn cEncode(
    input: [*]c_int,
    input_len: usize,
    output: [*]c_char,
    output_len: usize,
) callconv(.C) usize {
    const in: [*]u21 = @ptrCast(input);
    const out: [*]u8 = @ptrCast(output);
    const s = encode(
        in[0..input_len],
        out[0..output_len],
    ) catch return 0;
    return s.len;
}

fn cDecode(
    input: [*]c_char,
    input_len: usize,
    output: [*]c_int,
    output_len: usize,
) callconv(.C) usize {
    const in: [*]u8 = @ptrCast(input);
    const out: [*]u21 = @ptrCast(output);
    const s = decode(
        in[0..input_len],
        out[0..output_len],
    ) catch return 0;
    return s.len;
}

const std = @import("std");

const BASE = 36;
const TMIN = 1;
const TMAX = 26;
const SKEW = 38;
const DAMP = 700;
const INITIAL_BIAS = 72;
const INITIAL_N = 0x80;
const DELIMITER = 0x2D;

const Error = error{
    BadInput,
    BigOutput,
    Overflow,
};

const Flag = enum(u1) {
    none,
    uppercase,
};

inline fn isBasic(cp: u26) bool {
    return cp < 0x80;
}

inline fn isDelimiter(cp: u26) bool {
    return cp == DELIMITER;
}

fn decodeDigit(cp: u26) u26 {
    if (cp - 48 < 10) {
        return cp - 22;
    } else if (cp - 65 < 26) {
        return cp - 65;
    } else if (cp - 97 < 26) {
        return cp - 97;
    } else return BASE;
}

const expectEqual = std.testing.expectEqual;

test "decode digit" {
    try expectEqual(@as(u26, 0), decodeDigit('a'));
    try expectEqual(@as(u26, 0), decodeDigit('A'));
    try expectEqual(@as(u26, 25), decodeDigit('z'));
    try expectEqual(@as(u26, 26), decodeDigit('0'));
    try expectEqual(@as(u26, 35), decodeDigit('9'));
}

fn encodeDigit(d: u26, flag: Flag) u8 {
    const sub = (if (flag == .uppercase) @as(u8, 1) else 0) << 5;
    const ret = d + 22 + (if (d < 26) @as(u8, 75) else 0) - sub;
    std.debug.assert(0 <= ret);
    std.debug.assert(ret <= std.math.maxInt(u8));
    return @intCast(ret);
}

test "encode digit" {
    try expectEqual(@as(u8, 'a'), encodeDigit(0, .none));
    try expectEqual(@as(u8, 'A'), encodeDigit(0, .uppercase));
    try expectEqual(@as(u8, 'z'), encodeDigit(25, .none));
    try expectEqual(@as(u8, '0'), encodeDigit(26, .none));
    try expectEqual(@as(u8, '9'), encodeDigit(35, .none));
}

inline fn isFlagged(bcp: u26) bool {
    return bcp - 65 < 26;
}

test "is flagged" {
    try expectEqual(false, isFlagged(encodeDigit(0, .none)));
    try expectEqual(true, isFlagged(encodeDigit(0, .uppercase)));
}

fn encodeBasic(bcp: u26, flag: Flag) u8 {
    std.debug.assert(isBasic(bcp));
    const bcp_ = if (bcp - 97 < 26) bcp - (1 << 5) else bcp;
    const ret = if (flag == .none and (bcp_ - 65 < 26)) bcp_ + (1 << 5) else bcp_;
    std.debug.assert(0 <= ret);
    std.debug.assert(ret <= std.math.maxInt(u8));
    return @intCast(ret);
}

test "encode basic" {
    try expectEqual(@as(u8, 'a'), encodeBasic('a', .none));
    try expectEqual(@as(u8, 'A'), encodeBasic('a', .uppercase));
}

// https://tools.ietf.org/html/rfc3492#section-3.4
fn adapt(delta: u26, npoints: u26, first: bool) u26 {
    var d = if (first) delta / DAMP else delta / 2;
    d += d / npoints;

    var k: u26 = 0;
    while (d > (BASE - TMIN) * TMAX / 2) : (k += BASE)
        d /= BASE - TMIN;

    return k + (BASE - TMIN + 1) * d / (d + SKEW);
}

test "adapt" {
    _ = adapt(1, 3, false);
}

pub fn encode(input: []const u21, output: []u8) Error![]u8 {
    var n: u26 = INITIAL_N;
    var out: u26 = 0;
    var bias: u26 = INITIAL_BIAS;

    var j: usize = 0;
    while (j < input.len) : (j += 1) {
        if (isBasic(input[j])) {
            if (output.len - out < 2)
                return Error.BigOutput;
            output[out] = @intCast(input[j]);
            out += 1;
        } else if (input[j] > 0x10FFFF or (input[j] >= 0xD800 and input[j] <= 0xDBFF))
            return Error.BadInput;
    }

    const b = out;
    var h = b;

    if (0 < b) {
        output[out] = DELIMITER;
        out += 1;
    }

    var delta: u26 = 0;
    while (h < input.len) {
        var m: u26 = std.math.maxInt(u26);
        j = 0;
        while (j < input.len) : (j += 1) {
            if (input[j] >= n and input[j] < m)
                m = input[j];
        }

        if (m - n > (std.math.maxInt(u26) - delta) / (h + 1))
            return Error.Overflow;
        delta += (m - n) * (h + 1);
        n = m;

        j = 0;
        while (j < input.len) : (j += 1) {
            if (input[j] < n) {
                delta +%= 1;
                if (delta == 0)
                    return Error.Overflow;
            }

            if (input[j] == n) {
                var q: u26 = delta;
                var k: u26 = BASE;
                while (true) : (k += BASE) {
                    if (out >= output.len)
                        return Error.BigOutput;
                    const t = if (k <= bias) TMIN else if (k >= bias + TMAX) TMAX else k - bias;
                    if (q < t)
                        break;
                    output[out] = encodeDigit(t + (q - t) % (BASE - t), .none);
                    out += 1;
                    q = (q - t) / (BASE - t);
                }
                output[out] = encodeDigit(q, .none);
                out += 1;
                bias = adapt(delta, h + 1, h == b);
                delta = 0;
                h += 1;
            }
        }
        delta += 1;
        n += 1;
    }

    return output[0..out];
}

fn testEncode(input: []const u8, output: []const u8) !void {
    var in: [1024]u21 = undefined;
    var i: usize = 0;
    var j: usize = 0;
    while (i < input.len) {
        const oldi = i;
        i += std.unicode.utf8ByteSequenceLength(input[i]) catch unreachable;
        in[j] = try std.unicode.utf8Decode(input[oldi..i]);
        j += 1;
    }
    var out: [4096]u8 = undefined;
    const encoded = try encode(in[0..j], &out);
    const encoded_lower = try std.ascii.allocLowerString(std.testing.allocator, encoded);
    defer std.testing.allocator.free(encoded_lower);
    const output_lower = try std.ascii.allocLowerString(std.testing.allocator, output);
    defer std.testing.allocator.free(output_lower);
    try std.testing.expectEqualSlices(u8, encoded_lower, output_lower);
}

test "encode" {
    inline for (TESTS) |pair| {
        try testEncode(pair[0], pair[1]);
    }
}

test "encode bad input" {
    var in: [1]u21 = .{0x110000};
    var out: [1]u8 = undefined;
    try expectError(Error.BadInput, encode(&in, &out));
}

test "encode big output" {
    var in: [3]u21 = .{ 1, 2, 3 };
    var out: [1]u8 = undefined;
    try expectError(Error.BigOutput, encode(&in, &out));
}

pub fn decode(input: []const u8, output: []u21) Error![]u21 {
    var n: u26 = INITIAL_N;
    var out: u26 = 0;
    var i: u26 = 0;
    var bias: u26 = INITIAL_BIAS;
    const b = std.mem.lastIndexOfScalar(u8, input, DELIMITER) orelse 0;
    var j: usize = 0;

    if (b > output.len)
        return Error.BigOutput;

    while (j < b) : (j += 1) {
        if (!isBasic(input[j]))
            return Error.BadInput;
        output[out] = input[j];
        out += 1;
    }

    // TODO: make validation optional?
    j = if (0 < b) b + 1 else b;
    while (j < input.len) : (j += 1) {
        if (!isBasic(input[j]))
            return Error.BadInput;
    }

    var in: usize = if (0 < b) b + 1 else 0;

    while (in < input.len) : (out += 1) {
        const oldi = i;
        var w: u26 = 1;
        var k: u26 = BASE;
        while (true) : (k += BASE) {
            if (in >= input.len)
                return Error.BadInput;
            const digit = decodeDigit(input[in]);
            in += 1;
            if (digit > (std.math.maxInt(u26) - i) / w)
                return Error.Overflow;
            i += digit * w;
            const t = if (k <= bias) TMIN else if (k >= bias + TMAX) TMAX else k - bias;
            if (digit < t)
                break;
            if (w > std.math.maxInt(u26) / (BASE - t))
                return Error.Overflow;
            w *= BASE - t;
        }

        bias = adapt(i - oldi, out + 1, oldi == 0);

        if (i / (out + 1) > std.math.maxInt(u26) - n)
            return Error.Overflow;

        n += i / (out + 1);

        if (n > 0x10FFFF or (n >= 0xD800 and n <= 0xDBFF))
            return Error.BadInput;
        i %= (out + 1);

        if (out >= output.len)
            return Error.BigOutput;

        _ = memmove(u21, output[i + 1 ..], output[i..], out - i);
        output[i] = @intCast(n);
        i += 1;
    }

    return output[0..out];
}

fn testDecode(input: []const u8, output: []const u8) !void {
    var out: [1024]u21 = undefined;
    var utf8: [4096]u8 = undefined;
    const slajs = try decode(input, &out);
    var i: usize = 0;
    for (slajs) |cp| {
        var buf: [4]u8 = undefined;
        const l = try std.unicode.utf8Encode(@intCast(cp), &buf);
        for (buf[0..l]) |b| {
            utf8[i] = b;
            i += 1;
        }
    }
    try std.testing.expectEqualSlices(u8, output, utf8[0..i]);
}

test "decode" {
    inline for (TESTS) |pair| {
        try testDecode(pair[1], pair[0]);
    }
}

const expectError = std.testing.expectError;

test "decode bad input" {
    var out: [10]u21 = undefined;
    try expectError(Error.BadInput, decode("hej-å", &out));
}

test "decode big output" {
    var out: [1]u21 = undefined;
    try expectError(Error.BigOutput, decode("hej-", &out));
}

fn memmove(comptime T: type, dest: []T, src: []const T, n: usize) []T {
    if (@intFromPtr(dest.ptr) < @intFromPtr(src.ptr)) {
        var index: usize = 0;
        while (index != n) : (index += 1) {
            dest[index] = src[index];
        }
    } else {
        var index = n;
        while (index != 0) {
            index -= 1;
            dest[index] = src[index];
        }
    }

    return dest;
}

const TESTS = .{
    // https://tools.ietf.org/html/rfc3492#section-7.1)
    .{
        "\u{0644}\u{064A}\u{0647}\u{0645}\u{0627}\u{0628}\u{062A}\u{0643}\u{0644}\u{0645}\u{0648}\u{0634}\u{0639}\u{0631}\u{0628}\u{064A}\u{061F}",
        "egbpdaj6bu4bxfgehfvwxn",
    },
    .{
        "\u{4ED6}\u{4EEC}\u{4E3A}\u{4EC0}\u{4E48}\u{4E0D}\u{8BF4}\u{4E2D}\u{6587}",
        "ihqwcrb4cv8a8dqg056pqjye",
    },
    .{
        "\u{4ED6}\u{5011}\u{7232}\u{4EC0}\u{9EBD}\u{4E0D}\u{8AAA}\u{4E2D}\u{6587}",
        "ihqwctvzc91f659drss3x8bo0yb",
    },
    .{
        "\u{0050}\u{0072}\u{006F}\u{010D}\u{0070}\u{0072}\u{006F}\u{0073}\u{0074}\u{011B}\u{006E}\u{0065}\u{006D}\u{006C}\u{0075}\u{0076}\u{00ED}\u{010D}\u{0065}\u{0073}\u{006B}\u{0079}",
        "Proprostnemluvesky-uyb24dma41a",
    },
    .{
        "\u{05DC}\u{05DE}\u{05D4}\u{05D4}\u{05DD}\u{05E4}\u{05E9}\u{05D5}\u{05D8}\u{05DC}\u{05D0}\u{05DE}\u{05D3}\u{05D1}\u{05E8}\u{05D9}\u{05DD}\u{05E2}\u{05D1}\u{05E8}\u{05D9}\u{05EA}",
        "4dbcagdahymbxekheh6e0a7fei0b",
    },
    .{
        "\u{092F}\u{0939}\u{0932}\u{094B}\u{0917}\u{0939}\u{093F}\u{0928}\u{094D}\u{0926}\u{0940}\u{0915}\u{094D}\u{092F}\u{094B}\u{0902}\u{0928}\u{0939}\u{0940}\u{0902}\u{092C}\u{094B}\u{0932}\u{0938}\u{0915}\u{0924}\u{0947}\u{0939}\u{0948}\u{0902}",
        "i1baa7eci9glrd9b2ae1bj0hfcgg6iyaf8o0a1dig0cd",
    },
    .{
        "\u{306A}\u{305C}\u{307F}\u{3093}\u{306A}\u{65E5}\u{672C}\u{8A9E}\u{3092}\u{8A71}\u{3057}\u{3066}\u{304F}\u{308C}\u{306A}\u{3044}\u{306E}\u{304B}",
        "n8jok5ay5dzabd5bym9f0cm5685rrjetr6pdxa",
    },
    .{
        "\u{C138}\u{ACC4}\u{C758}\u{BAA8}\u{B4E0}\u{C0AC}\u{B78C}\u{B4E4}\u{C774}\u{D55C}\u{AD6D}\u{C5B4}\u{B97C}\u{C774}\u{D574}\u{D55C}\u{B2E4}\u{BA74}\u{C5BC}\u{B9C8}\u{B098}\u{C88B}\u{C744}\u{AE4C}",
        "989aomsvi5e83db1d2a355cv1e0vak1dwrv93d5xbh15a0dt30a5jpsd879ccm6fea98c",
    },
    .{
        "\u{043F}\u{043E}\u{0447}\u{0435}\u{043C}\u{0443}\u{0436}\u{0435}\u{043E}\u{043D}\u{0438}\u{043D}\u{0435}\u{0433}\u{043E}\u{0432}\u{043E}\u{0440}\u{044F}\u{0442}\u{043F}\u{043E}\u{0440}\u{0443}\u{0441}\u{0441}\u{043A}\u{0438}",
        "b1abfaaepdrnnbgefbaDotcwatmq2g4l",
    },
    .{
        "\u{0050}\u{006F}\u{0072}\u{0071}\u{0075}\u{00E9}\u{006E}\u{006F}\u{0070}\u{0075}\u{0065}\u{0064}\u{0065}\u{006E}\u{0073}\u{0069}\u{006D}\u{0070}\u{006C}\u{0065}\u{006D}\u{0065}\u{006E}\u{0074}\u{0065}\u{0068}\u{0061}\u{0062}\u{006C}\u{0061}\u{0072}\u{0065}\u{006E}\u{0045}\u{0073}\u{0070}\u{0061}\u{00F1}\u{006F}\u{006C}",
        "PorqunopuedensimplementehablarenEspaol-fmd56a",
    },
    .{
        "\u{0054}\u{1EA1}\u{0069}\u{0073}\u{0061}\u{006F}\u{0068}\u{1ECD}\u{006B}\u{0068}\u{00F4}\u{006E}\u{0067}\u{0074}\u{0068}\u{1EC3}\u{0063}\u{0068}\u{1EC9}\u{006E}\u{00F3}\u{0069}\u{0074}\u{0069}\u{1EBF}\u{006E}\u{0067}\u{0056}\u{0069}\u{1EC7}\u{0074}",
        "TisaohkhngthchnitingVit-kjcr8268qyxafd2f1b9g",
    },
    .{
        "\u{0033}\u{5E74}\u{0042}\u{7D44}\u{91D1}\u{516B}\u{5148}\u{751F}",
        "3B-ww4c5e180e575a65lsy2b",
    },
    .{
        "\u{5B89}\u{5BA4}\u{5948}\u{7F8E}\u{6075}\u{002D}\u{0077}\u{0069}\u{0074}\u{0068}\u{002D}\u{0053}\u{0055}\u{0050}\u{0045}\u{0052}\u{002D}\u{004D}\u{004F}\u{004E}\u{004B}\u{0045}\u{0059}\u{0053}",
        "-with-SUPER-MONKEYS-pc58ag80a8qai00g7n9n",
    },
    .{
        "\u{0048}\u{0065}\u{006C}\u{006C}\u{006F}\u{002D}\u{0041}\u{006E}\u{006F}\u{0074}\u{0068}\u{0065}\u{0072}\u{002D}\u{0057}\u{0061}\u{0079}\u{002D}\u{305D}\u{308C}\u{305E}\u{308C}\u{306E}\u{5834}\u{6240}",
        "Hello-Another-Way--fc4qua05auwb3674vfr0b",
    },
    .{
        "\u{3072}\u{3068}\u{3064}\u{5C4B}\u{6839}\u{306E}\u{4E0B}\u{0032}",
        "2-u9tlzr9756bt3uc0v",
    },
    .{
        "\u{004D}\u{0061}\u{006A}\u{0069}\u{3067}\u{004B}\u{006F}\u{0069}\u{3059}\u{308B}\u{0035}\u{79D2}\u{524D}",
        "MajiKoi5-783gue6qz075azm5e",
    },
    .{
        "\u{30D1}\u{30D5}\u{30A3}\u{30FC}\u{0064}\u{0065}\u{30EB}\u{30F3}\u{30D0}",
        "de-jg4avhby1noc0d",
    },
    .{
        "\u{305D}\u{306E}\u{30B9}\u{30D4}\u{30FC}\u{30C9}\u{3067}",
        "d9juau41awczczp",
    },
    .{
        "\u{002D}\u{003E}\u{0020}\u{0024}\u{0031}\u{002E}\u{0030}\u{0030}\u{0020}\u{003C}\u{002D}",
        "-> $1.00 <--",
    },
};
