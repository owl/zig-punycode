# zig-punycode

Build status: [![builds.sr.ht status](https://builds.sr.ht/~alva/zig-punycode.svg)](https://builds.sr.ht/~alva/zig-punycode?)

Based on the example implementation in [RFC-3492](https://tools.ietf.org/html/rfc3492).
